-- :name create-page! :! :n
-- :doc creates a new page
insert into appPage
(page_name, page_type)
values (:page_name, :page_type::pageType)

-- :name delete-page! :! :n
-- :doc deletes a page
delete from appPage
where page_id = :page_id::integer

-- :name reorder-page! :! :n
-- :doc updates the order_key of one page
update appPage
set order_key = :order_key
where page_id = :page_id

-- :name add-task! :! :n
-- :doc add a task
insert into task
(task_name, page_ref, highlight)
values (:task_name, :page_ref::integer, :highlight)

-- :name delete-task! :! :n
-- :doc delete one or more tasks
delete from task
where task_id = any(array[:v*:task_ids]::integer[])

-- :name list-pages :? :*
-- :doc lists pages in order
select * from appPage
order by order_key

-- :name get-page :? :1
-- :doc gets the info for a single page
select * from appPage
where page_name = :page_name

-- :name get-default-page :? :1
-- :doc gets the name of the page with the lowest index
select page_name from appPage
order by order_key
limit 1

-- :name list-due-tasks :? :*
-- :doc lists the tasks for a page
select * from task
where page_ref = :page_ref
and defcat_named is NULL
and defcat_dated is NULL
order by sort_id asc, task_id asc;

-- :name list-defcats-named :? :*
-- :doc get all the defcats that need to be displayed on a page
select distinct
    cat_id,
    cat_name,
    order_key
from defCatNamed
inner join task
    on cat_id = defcat_named
where page_ref = :page_ref::integer
--~ (when (:cat_name params) "and cat_name = :cat_name")
order by order_key

-- :name list-defcats-dated :? :*
-- :doc get all the defcats that need to be displayed on a page
select distinct
    cat_id,
    def_date,
    order_key
from defCatDated
inner join task
    on cat_id = defcat_dated
where page_ref = :page_ref::integer
--~ (when (:def_date params) "and def_date = :def_date::date")
order by def_date

-- :name get-task-info :? :*
-- :doc get all the info for a list of tasks
select * from task
where task_id = any(array[:v*:task_ids]::integer[])
order by task_id

-- :name list-all-tasks :? :*
-- :doc list all task deets for a page
select * from task
where page_ref = :page_ref::integer
order by task_id

-- :name tasks-defcat-named :? :*
-- :doc list all the tasks for a category
select * from task
where defcat_named = :defcat_ref

-- :name tasks-defcat-dated :? :*
-- :doc list all the tasks for a category
select * from task
where defcat_dated = :defcat_ref

-- :name create-defcat-named! :<! :1
-- :doc create a defCatNamed with a name
insert into defCatNamed
(cat_name)
values (:cat_name)
returning cat_id

-- :name create-defcat-dated! :<! :1
-- :doc create a defCatDated with a date
insert into defCatDated
(def_date)
values (:def_date::date)
returning cat_id

-- :name delete-defcat-named! :! :n
-- :doc delete a defCatNamed
delete from defCatNamed
where cat_id = :cat_id

-- :name delete-defcat-dated! :! :n
-- :doc delete a defCatDated
delete from defCatDated
where cat_id = :cat_id

-- :name defer-task-named! :! :n
-- :doc assign tasks to a defCatNamed
update task
set defcat_named = :defcat_ref::integer,
    highlight = NULL
where task_id = any(array[:v*:task_ids]::integer[])

-- :name defer-task-dated! :! :n
-- :doc assign tasks to a defCatNamed
update task
set defcat_dated = :defcat_ref::integer,
    highlight = NULL
where task_id = any(array[:v*:task_ids]::integer[])

-- :name undefer-task-named! :! :n
-- :doc undefer a task
update task
set defcat_named = NULL
where task_id = any(array[:v*:task_ids]::integer[])

-- :name undefer-task-dated! :! :n
-- :doc undefer a task
update task
set defcat_dated = NULL
where task_id = any(array[:v*:task_ids]::integer[])

-- :name move-task! :! :n
-- :doc moves one-or-more tasks to a new page
update task
set page_ref = :newpage::integer
where task_id = any(array[:v*:task_ids]::integer[])

-- :name add-habit! :! :n
-- :doc add a habit
insert into habit
(habit_name, page_ref, freq_unit, freq_value)
values (:habit_name, :page_ref::integer, :freq_unit::timeUnit, :freq_value::integer)

-- :name list-due-habits :? :*
-- :doc get all due habits, ordered by due date
select * from habit
where page_ref = :page_ref
and date_scheduled <= CURRENT_DATE
order by sort_id asc, date_scheduled asc, habit_id asc;

-- :name list-upcoming-habits :? :*
-- :doc get all upcoming habits, ordered by due date
select * from habit
where page_ref = :page_ref
and date_scheduled > CURRENT_DATE
order by date_scheduled

-- :name get-habit-info :? :*
-- :doc get all the info for a list of habits
select * from habit
where habit_id = any(array[:v*:habit_ids]::integer[])
order by date_scheduled

-- :name list-all-habits :? :*
-- :doc list all habit deets for a page
select * from habit
where page_ref = :page_ref::integer
order by date_scheduled

-- :name get-habit-deets :? :1
-- :doc just get all the deets for one habit
select * from habit
where habit_id = :habit_id::integer

-- :name defer-habit! :! :n
-- :doc defer a habit to a specific date
update habit
set date_scheduled = :defer_date::date,
    highlight = NULL
/*~
(if (:done params)
  ",\n    last_done = CURRENT_DATE")
~*/
where habit_id = any(array[:v*:habit_ids]::integer[])

-- :name delete-habit! :! :n
-- :doc delete one or more habits
delete from habit
where habit_id = any(array[:v*:habit_ids]::integer[])

-- :name create-user! :! :n
-- :doc creates a new user with the provided login and hashed password
insert into users
(login, password)
values (:login, :password)

-- :name get-user-for-auth :? :1
-- :doc selects a user for authentication
select * from users
where login = :login

-- :name modify-habit! :! :n
-- :doc modifies a single habit
update habit
set habit_name = :habit_name,
freq_value = :freq_value::integer,
freq_unit = :freq_unit::timeUnit,
date_scheduled = :date_scheduled::date
where habit_id = :habit_id::integer

-- :name modify-task! :! :n
-- :doc modifies a single task
update task
set task_name = :task_name
where task_id = :task_id::integer

-- :name highlight-tasks! :! :n
-- :doc set the highlight for one or more tasks
update task
set highlight = :highlight
where task_id = any(array[:v*:task_ids]::integer[])

-- :name highlight-habits! :! :n
-- :doc set the highlight for one or more habits
update habit
set highlight = :highlight
where habit_id = any(array[:v*:habit_ids]::integer[])

-- :name undefer-highlight! :! :n
-- :doc highlights all tasks for a given category a special color
update task
set highlight = 'khaki'
where defcat_dated = :cat_id::integer

-- :name move-habit! :! :n
-- :doc moves one-or-more habits to a new page
update habit
set page_ref = :newpage::integer
where habit_id = any(array[:v*:habit_ids]::integer[])

-- :name distinct-task-highlights :? :*
select distinct highlight
from task
where page_ref = :page_ref::integer

-- :name distinct-habit-highlights :? :*
select distinct highlight
from habit
where page_ref = :page_ref::integer

-- :name sort-tasks! :! :n
-- :doc sort tasks
update task
set sort_id =
  case
/*~
(for [[h i] (:mappings params)]
  (str "when highlight = '" h "' then " i "\n"))
~*/
  end
where page_ref = :page_ref::integer

-- :name sort-habits! :! :n
-- :doc sort habits
update habit
set sort_id =
  case
/*~
(for [[h i] (:mappings params)]
  (str "when highlight = '" h "' then " i "\n"))
~*/
  end
where page_ref = :page_ref::integer
