(ns todefer.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [todefer.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[todefer started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[todefer has shut down successfully]=-"))
   :middleware wrap-dev})
