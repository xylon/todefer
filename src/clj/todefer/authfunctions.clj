(ns todefer.authfunctions
  (:require
   [buddy.hashers :as hashers]
   [todefer.db.core :as db]))

(defn create-user! [login password]
  (if-not (empty? (db/get-user-for-auth {:login login}))
    (throw (ex-info "User already exists!"
                    {:todefer/error-id ::duplicate-user
                     :error "User already exists!"}))
    (db/create-user! {:login login :password (hashers/derive password)})))

(defn authenticate-user [login password]
  (let [{hashed :password :as user} (db/get-user-for-auth {:login login})]
    (when (hashers/check password hashed)
      (dissoc user :password))))
