(ns todefer.sharedfunctions
  (:require
   [cuerdas.core :as cstr]
   [struct.core :as st]))

(def colormap
  {"red" "lightcoral"
   "green" "lightgreen"
   "blue" "lightblue"
   "none" nil})

(def valid-freq-units
  #{"days" "weeks" "months" "years"})

(def valid-page-types
  #{"task" "habit"})

(defn validate-and-exec "validate with the supplied schema and execute"
  [params schema execute]
  (let [validated (st/validate params schema {:strip true})]
    (when-not (validated 0)
      (execute (validated 1)))))

(defn numornumstr? "is it a number or a numeric string?"
  [item]
  (or (number? item)
      (and (string? item) (cstr/numeric? item))))

(defn isodate? "is it a string formatted as an iso date?"
  [item]
  (and (string? item)
       (re-matches #"\d{4}-\d{2}-\d{2}" item)))

(defn non-empty-string? "is it a non-empty string"
  [item]
  (and (string? item)
       (not-empty item)))

(def colorvalidator
  {:message "color not recognized"
   :optional true
   :validate #(contains? colormap %)})

(def frequnit_validator
  {:message "invalid unit of time"
   :optional true
   :validate #(contains? valid-freq-units %)})

(def isodate_validator
  {:message "this does not look like an ISO date"
   :optional true
   :validate isodate?})

(def pagetype_validator
  {:message "invalid page type"
   :optional true
   :validate #(contains? valid-page-types %)})

(def id_list_validator
  {:message "id(s) not valid"
   :optional true
   :validate #(or (numornumstr? %)
                  (and
                   (vector? %)
                   (not-empty %)
                   (every? numornumstr? %)))
   :coerce #(if (vector? %) % [%])})

(def string_list_validator
  {:message "string(s) not valid"
   :optional true
   :validate #(or (string? %)
                  (and
                   (vector? %)
                   (not-empty %)
                   (every? non-empty-string? %)))
   :coerce #(if (vector? %) % [%])})

(def frequnit_list_validator
  {:message "freq_unit(s) not valid"
   :optional true
   :validate #(or (valid-freq-units %)
                  (and
                   (vector? %)
                   (not-empty %)
                   (every? valid-freq-units %)))
   :coerce #(if (vector? %) % [%])})

(def isodate_list_validator
  {:message "date(s) not valid"
   :optional true
   :validate #(or (isodate? %)
                  (and
                   (vector? %)
                   (not-empty %)
                   (every? isodate? %)))
   :coerce #(if (vector? %) % [%])})

(defn always-vector
  "this function takes something which may or may not be a vector and makes it always a vector"
  [item]
  (cond
    (vector? item) item
    (nil? item) []
    :else [item]))
