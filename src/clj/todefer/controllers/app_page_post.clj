(ns todefer.controllers.app-page-post
  (:require
   [todefer.db.core :as db]
   [java-time :as jt]
   [todefer.sharedfunctions :as shared]
   [struct.core :as st]))

(defmacro map-of
  [& xs]
  `(hash-map ~@(mapcat (juxt keyword identity) xs)))

(def timeunit-multipliers
  {"days" 1
   "weeks" 7
   "months" 28
   "years" 365})

(defn done-habit [habit_id offset]
  (let [{:keys [freq_unit freq_value]} (db/get-habit-deets {:habit_id habit_id})
        dayshence (jt/days (* (get timeunit-multipliers freq_unit) freq_value))
        now (jt/minus (jt/local-date) (jt/days offset))]
    (db/defer-habit! {:habit_ids [habit_id]
                      :defer_date (jt/plus now dayshence)
                      :done true})))

(defn log
  "concatenate and save to file"
  [& strings]
  (spit "/tmp/cljdebug.txt" (str (reduce str strings) "
")))

;; (spit "/home/joseph/cljdebug.txt" )

(defmulti app-page-post (fn [request] (-> request :params :action)))

(comment
  (defmethod app-page-post ""  ;; 
    [{:keys [params]}]
    (shared/validate-and-exec
     params
     {}
     #()))
)

;; tasks

(defmethod app-page-post "add_task"  ;; add a task
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_name [st/required st/string]
    :page_ref  [st/required st/integer-str]}
   #(db/add-task! (assoc % :highlight nil))))

(defmethod app-page-post "add_task_highlighted"  ;; add a task, highlighted
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_name [st/required st/string]
    :page_ref  [st/required st/integer-str]
    :color     [st/required shared/colorvalidator]}
   #(let [htmlcolor (get shared/colormap (% :color))]
      (db/add-task! (assoc % :highlight htmlcolor)))))

(defmethod app-page-post "defer_existing"  ;; defer to an existing category
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_id [st/required shared/id_list_validator]
    :defcat  [st/required st/string]}
   #(let [{:keys [defcat task_id]} %
          cat_type (subs defcat 0 5)
          cat_ref (subs defcat 6)]
      (case cat_type
        "named" (db/defer-task-named! {:task_ids task_id :defcat_ref cat_ref})
        "dated" (db/defer-task-dated! {:task_ids task_id :defcat_ref cat_ref})))))

(defmethod app-page-post "defer_new"  ;; defer to a new category
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_id  [st/required shared/id_list_validator]
    :new_name [st/required st/string]
    :page_ref [st/required st/integer-str]}
   #(let [{:keys [task_id new_name page_ref]} %
          existing_cats (db/list-defcats-named {:page_ref page_ref
                                                :cat_name new_name})]
      ;; if there is not already a category by the same name
      (if (empty? existing_cats)
        (let [cat_id ((db/create-defcat-named! {:cat_name new_name}) :cat_id)]
          (db/defer-task-named! {:task_ids task_id :defcat_ref cat_id}))
        (let [cat_id (-> existing_cats first :cat_id)]
          (db/defer-task-named! {:task_ids task_id :defcat_ref cat_id}))))))

(defmethod app-page-post "defer_date"  ;; defer to a date
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_id  [st/required shared/id_list_validator]
    :new_date [st/required shared/isodate_validator]
    :page_ref [st/required st/integer-str]}
   #(let [{:keys [task_id new_date page_ref]} %
          sanitized_taskids task_id
          existing_cats (db/list-defcats-dated {:page_ref page_ref :def_date new_date})]
      ;; if there is not already a category by the same date
      (if (= (count existing_cats) 0)
        (let [cat_id ((db/create-defcat-dated! {:def_date new_date}) :cat_id)]
          (db/defer-task-dated! {:task_ids sanitized_taskids :defcat_ref cat_id}))
        (let [cat_id (-> existing_cats first :cat_id)]
          (db/defer-task-dated! {:task_ids sanitized_taskids :defcat_ref cat_id}))))))

(defmethod app-page-post "undefer-named"  ;; undefer a task which was deferred to a name
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_id [st/required shared/id_list_validator]}
   #(let [{:keys [task_id]} %]
      (db/undefer-task-named! {:task_ids task_id}))))

(defmethod app-page-post "undefer-dated"  ;; undefer a task which was deferred to a date
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_id [st/required shared/id_list_validator]}
   #(let [{:keys [task_id]} %]
      (db/undefer-task-dated! {:task_ids task_id}))))

(defmethod app-page-post "delete_task"  ;; delete a task
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_id [st/required shared/id_list_validator]}
   #(let [{:keys [task_id]} %]
      (db/delete-task! {:task_ids task_id}))))

(defn modify-task [task_id task_name]
  (db/modify-task! (map-of task_id task_name)))

(defmethod app-page-post "save_changes_tasks"  ;; multi-edit of tasks
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_id   [st/required shared/id_list_validator]
    :task_name [st/required shared/string_list_validator]}
   #(let [{:keys [task_id
                  task_name]} %]
      (doall (apply map modify-task [task_id task_name])))))

(defmethod app-page-post "highlight_task_2"  ;; highlight a task
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_id   [st/required shared/id_list_validator]
    :color     [st/required shared/colorvalidator]}
   #(let [{:keys [task_id color]} %]
      (db/highlight-tasks! {:task_ids task_id
                            :highlight (get shared/colormap color)}))))

(defmethod app-page-post "move_task_to_page"  ;; move a task to a different page
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:task_id   [st/required shared/id_list_validator]
    :newpage   [st/required st/integer-str]}
   #(let [{:keys [task_id newpage]} %]
      (db/move-task! {:task_ids task_id
                      :newpage newpage}))))

(defmethod app-page-post "sort_tasks"  ;; sort tasks
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:page_ref [st/required st/integer-str]}
   #(let [{:keys [page_ref]} %
          highlights (map :highlight (db/distinct-task-highlights {:page_ref page_ref}))
          ;; basically we're converting the strings to incrementing numbers
          mappings (zipmap highlights (range))]
      (db/sort-tasks! {:page_ref page_ref
                       :mappings mappings}))))

;; habits

(comment
  (defmethod app-page-post ""  ;; 
    [{:keys [params]}]
    (shared/validate-and-exec
     params
     {}
     #()))
  )

(defmethod app-page-post "add_habit"  ;; add a habit
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:page_ref   [st/required st/integer-str]
    :habit_name [st/required st/string]
    :freq_value [st/required st/integer-str]
    :freq_unit  [st/required shared/frequnit_validator]}
   #(let [{:keys [page_ref habit_name freq_value freq_unit]} %]
      (db/add-habit! (map-of page_ref habit_name freq_value freq_unit)))))

(defmethod app-page-post "done"  ;; marks a habit done
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:habit_id [st/required shared/id_list_validator]
    :donewhen [st/required st/string]}
   #(let [{:keys [habit_id donewhen]} %
          done-habit' (fn [id] (done-habit id (case donewhen
                                                "today" 0
                                                "yesturday" 1)))]
      (run! done-habit' habit_id))))

(defmethod app-page-post "delete_habit"  ;; delete one-or-more habits
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:habit_id [st/required shared/id_list_validator]}
   #(let [{:keys [habit_id]} %]
      (db/delete-habit! {:habit_ids habit_id}))))

(defmethod app-page-post "defer_habit"  ;; defer one-or-more habits
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:habit_id [st/required shared/id_list_validator]
    :defer_date [st/required shared/isodate_validator]}
   #(let [{:keys [habit_id defer_date]} %]
      (db/defer-habit! {:habit_ids habit_id
                        :defer_date defer_date}))))

(defn modify-habit [habit_id habit_name freq_value freq_unit date_scheduled]
  (db/modify-habit! (map-of habit_id habit_name freq_value freq_unit date_scheduled)))

(defmethod app-page-post "save_changes_habits"  ;; save changes to habits
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:habit_id       [st/required shared/id_list_validator]
    :habit_name     [st/required shared/string_list_validator]
    :freq_value     [st/required shared/id_list_validator]
    :freq_unit      [st/required shared/frequnit_list_validator]
    :date_scheduled [st/required shared/isodate_list_validator]}
   #(let [{:keys [habit_id
                  habit_name
                  freq_value
                  freq_unit
                  date_scheduled]} %]
      (doall (apply map modify-habit [habit_id habit_name freq_value freq_unit date_scheduled])))))

(defmethod app-page-post "highlight_habit_2"  ;; highlight one or more habits
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:habit_id [st/required shared/id_list_validator]
    :color    [st/required shared/colorvalidator]}
   #(let [{:keys [habit_id color]} %]
      (db/highlight-habits! {:habit_ids habit_id
                             :highlight (get shared/colormap color)}))))

(defmethod app-page-post "move_habit_to_page"  ;; move a habit to a different page
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:habit_id   [st/required shared/id_list_validator]
    :newpage   [st/required st/integer-str]}
   #(let [{:keys [habit_id newpage]} %]
      (db/move-habit! {:habit_ids habit_id
                       :newpage newpage}))))

(defmethod app-page-post "sort_habits"  ;; sort tasks
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:page_ref [st/required st/integer-str]}
   #(let [{:keys [page_ref]} %
          highlights (map :highlight (db/distinct-habit-highlights {:page_ref page_ref}))
          ;; basically we're converting the strings to incrementing numbers
          mappings (zipmap highlights (range))]
      (db/sort-habits! {:page_ref page_ref
                        :mappings mappings}))))

