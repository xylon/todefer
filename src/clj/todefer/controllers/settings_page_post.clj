(ns todefer.controllers.settings-page-post
  (:require
   [todefer.db.core :as db]
   [java-time :as jt]
   [todefer.sharedfunctions :as shared]
   [struct.core :as st]))

(defmacro map-of
  [& xs]
  `(hash-map ~@(mapcat (juxt keyword identity) xs)))

;; (spit "/home/joseph/cljdebug.txt" )

(defn reorderfunc [{:keys [page_id state newvec]} page]
  (case state
    "returning" (if (not= page page_id)
                  {:page_id page_id :state "returning" :newvec (conj newvec page)}
                  {:page_id page_id :state "swapping"  :newvec newvec})
    "swapping"    {:page_id page_id :state "returning" :newvec (conj newvec page page_id)}))

(defn apply-order [newlist]
  (doall (map-indexed (fn [index page_id]
                        (db/reorder-page! {:order_key index :page_id page_id})) newlist)))

(defmulti settings-page-post (fn [request] ((:params request) :action)))

(comment
  (defmethod settings-page-post ""  ;; 
    [{:keys [params]}]
    (shared/validate-and-exec
     params
     {}
     #()))
)

(defmethod settings-page-post "add_page"  ;; add a new page
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:page_name [st/required st/string]
    :page_type [st/required shared/pagetype_validator]}
   #(let [{:keys [page_name page_type]} %]
      (db/create-page! {:page_name page_name :page_type page_type}))))

(defmethod settings-page-post "delete_page"  ;; delete a page
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:page_id  [st/required st/integer-str]}
   #(let [{:keys [page_id]} %]
      (db/delete-page! {:page_id page_id}))))

(defmethod settings-page-post "move-down"  ;; re-order this page down
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:page_id  [st/required st/integer-str]}
   #(let [{:keys [page_id]} %
          page_list (mapv :page_id (db/list-pages))
          {reordered_list :newvec} (reduce reorderfunc {:page_id page_id :state "returning" :newvec []} page_list)]
      (apply-order reordered_list))))

(defmethod settings-page-post "move-up"  ;; re-order this page up
  [{:keys [params]}]
  (shared/validate-and-exec
   params
   {:page_id  [st/required st/integer-str]}
   #(let [{:keys [page_id]} %
          page_list (mapv :page_id (rseq (db/list-pages)))
          {reordered_list :newvec} (reduce reorderfunc {:page_id page_id :state "returning" :newvec []} page_list)]
      (apply-order (rseq reordered_list)))))
