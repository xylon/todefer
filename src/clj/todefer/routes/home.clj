(ns todefer.routes.home
  (:require
   [todefer.layout :as layout]
   [todefer.db.core :as db]
   [clojure.java.io :as io]
   [todefer.middleware :as middleware]
   [ring.util.response]
   [ring.util.http-response :as response]
   [java-time :as jt]
   [todefer.controllers.app-page-post :as capp]
   [todefer.controllers.settings-page-post :as cspp]
   [todefer.sharedfunctions :as shared]))

(defn urlencode [foo]
  (java.net.URLEncoder/encode foo "UTF-8"))

(defn urldecode [foo]
  (java.net.URLDecoder/decode foo "UTF-8"))

(defn add-sanity [apppage]
  (conj apppage {:page_name_sanitized (urlencode (apppage :page_name))}))

(defn add-tasks-named [defcat]
  (conj defcat {:tasks (db/tasks-defcat-named {:defcat_ref (defcat :cat_id)})}))

(defn add-tasks-dated [defcat]
  (conj defcat {:tasks (db/tasks-defcat-dated {:defcat_ref (defcat :cat_id)})}))

;; (spit "/home/joseph/cljdebug.txt" )

(defn undefer-due
  "to be used by reduce to either return a category back or undefer it"
  [buildme defcat]
  (let [{cat_id :cat_id
         catdate :def_date} defcat
        datenow (jt/local-date)]
    (if (jt/not-after? catdate datenow)
      (do (db/undefer-highlight! {:cat_id cat_id})
          (db/delete-defcat-dated! {:cat_id cat_id})
          buildme)
      (conj buildme defcat))))

(defn list-defcats-dated-undefer
  "basically a wrapper for db/list-defcats-dated that undefers categories as appropriate"
  [page_id]
  (let [defcats_dated (db/list-defcats-dated {:page_ref page_id})]
    (reduce undefer-due [] defcats_dated)))

(def days {"Mon" "Monday"
           "Tue" "Tuesday"
           "Wed" "Wednesday"
           "Thu" "Thursday"
           "Fri" "Friday"
           "Sat" "Saturday"
           "Sun" "Sunday"})

(defn prettify-due
  "we take a map, and we add a :prettydue based off of the key in datekey"
  [habitdata datekey]
  (let [{date_scheduled datekey} habitdata
        now (jt/local-date)]
    (conj habitdata {:prettydue (cond
                                  (= date_scheduled now)
                                    "today"
                                  (= date_scheduled (jt/plus now (jt/days 1)))
                                    "tomorrow"
                                  (= date_scheduled (jt/minus now (jt/days 1)))
                                    "yesturday"
                                  (jt/before? (jt/minus now (jt/days 6)) date_scheduled (jt/plus now (jt/days 6)))
                                    (days (jt/format "E" date_scheduled))
                                  :else
                                    date_scheduled)})))

(defn app-page
  "renders the tasks or habits page"
  [request]
  (let [page_name (-> request :path-params :pagename urldecode)
        {:keys [page_id page_type]} (db/get-page {:page_name page_name})]
    (case page_type
      "task"
      (let [defcatsnamed (map add-tasks-named (db/list-defcats-named {:page_ref page_id}))
            defcatsdated (map add-tasks-dated (map #(prettify-due % :def_date) (list-defcats-dated-undefer page_id)))]
        (layout/render request "task-page.html" {:apppages (map add-sanity (db/list-pages))
                                                 :page_name page_name
                                                 :page_name_sanitized (urlencode page_name)
                                                 :title page_name
                                                 :page_id page_id
                                                 :page_type page_type
                                                 :defcatsnamed defcatsnamed
                                                 :defcatsdated defcatsdated
                                                 :tasks (db/list-due-tasks {:page_ref page_id})
                                                 :highlightcolors (keys shared/colormap)}))
      "habit"
      (let [duehabits (map #(prettify-due % :date_scheduled) (db/list-due-habits {:page_ref page_id}))
            upcominghabits (map #(prettify-due % :date_scheduled) (db/list-upcoming-habits {:page_ref page_id}))]
        (layout/render request "habit-page.html" {:apppages (map add-sanity (db/list-pages))
                                                  :page_name page_name
                                                  :page_name_sanitized (urlencode page_name)
                                                  :title page_name
                                                  :page_id page_id
                                                  :page_type page_type
                                                  :duehabits duehabits
                                                  :upcominghabits upcominghabits
                                                  :highlightcolors (keys shared/colormap)})))))

(defn settings-page [request]
  (layout/render request "settings.html" {:apppages (map add-sanity (db/list-pages))
                                          :page_name "/settings_y7je3sig"
                                          :title "Settings"}))

(defn modify-habit-page [request]
  (let [{{:keys [habit_id page_ref]} :params
         {redirect "redirect" :or {redirect "/"}} :query-params} request]
    ;; have they selected anything?
    (if (seq habit_id)
      (let [habit_info (db/get-habit-info {:habit_ids (shared/always-vector habit_id)})]
        (layout/render request "modify-habit.html" {:apppages (map add-sanity (db/list-pages))
                                                    :page_name redirect
                                                    :title "Modify Habit"
                                                    :habits habit_info
                                                    :units ["days" "weeks" "months" "years"]
                                                    :redirectto redirect}))
      (let [habit_info (db/list-all-habits {:page_ref page_ref})]
        (layout/render request "modify-habit.html" {:apppages (map add-sanity (db/list-pages))
                                                    :page_name redirect
                                                    :title "Modify Habit"
                                                    :habits habit_info
                                                    :units ["days" "weeks" "months" "years"]
                                                    :redirectto redirect})))))

(defn modify-task-page [request]
  (let [{{:keys [task_id page_ref]} :params
         {redirect "redirect" :or {redirect "/"}} :query-params} request]
    ;; have they selected anything?
    (if (seq task_id)
      (let [task_info (db/get-task-info {:task_ids (shared/always-vector task_id)})]
        (layout/render request "modify-task.html" {:apppages (map add-sanity (db/list-pages))
                                                   :page_name redirect
                                                   :title "Modify Task"
                                                   :tasks task_info
                                                   :redirectto redirect}))
      (let [task_info (db/list-all-tasks {:page_ref page_ref})]
        (layout/render request "modify-task.html" {:apppages (map add-sanity (db/list-pages))
                                                   :page_name redirect
                                                   :title "Modify Task"
                                                   :tasks task_info
                                                   :redirectto redirect})))))

(defn redirect-default [request]
  (let [qresult (db/get-default-page)]
    (if (nil? qresult)
      (layout/render request "empty.html")
      (response/found (str "/app-page/" (-> qresult
                                            add-sanity
                                            :page_name_sanitized))))))

(defn redirect-back [request]
  (response/found (-> request :query-params (get "redirect"))))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats
                 middleware/wrap-auth]}
   ["/" {:get redirect-default}]
   ["/app-page/:pagename" {:get app-page
                           :post (fn [request]
                                   (capp/app-page-post request)
                                   (app-page request))}]
   ["/settings" {:get settings-page
                 :post (fn [request]
                         (cspp/settings-page-post request)
                         (settings-page request))}]
   ["/modifyhabit" {:post modify-habit-page
                    :get redirect-back}]
   ["/modifytask" {:post modify-task-page
                   :get redirect-back}]])

