(ns todefer.routes.auth
  (:require
   [todefer.layout :as layout]
   [todefer.authfunctions :as authfunc]
   [todefer.middleware :as middleware]
   [ring.util.response]
   [ring.util.http-response :as response]))

(defn login-page [request]
  (let [{{redirect "redirect" :or {redirect "/"}} :query-params} request
        redirectencoded (java.net.URLEncoder/encode redirect "UTF-8")]
    (layout/render request "login.html" {:redirect redirectencoded
                                         :title "Login"})))

(defn login-attempt [request]
  (let [{{:keys [username password]} :params
         {redirect "redirect" :or {redirect "/"}} :query-params} request
        redirectencoded (java.net.URLEncoder/encode redirect "UTF-8")
        session (request :session)
        authresult (authfunc/authenticate-user username password)]
    (if authresult
      (-> (response/found redirect)
          (assoc :session (assoc session :user (authresult :login))))
      (layout/render request "login.html" {:error (str "Authentication Failure")
                                           :redirect redirectencoded
                                           :title "Login"}))))

(defn logout-now [request]
  (let [{{redirect "redirect"} :query-params
         session :session} request]
    (-> (response/found (if (empty? redirect) "/" redirect))
        (assoc :session (dissoc session :user)))))

(defn auth-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/login" {:get login-page
              :post login-attempt}]
   ["/logout" {:get logout-now}]])
