(ns todefer.habits
  (:require [todefer.shared :refer [log byid byclass collapser]]))

(set! (.-onkeyup js/document)
      (fn [e]
        (when (.-ctrlKey e)
          (case (.-key e)
            "y" (.focus (byid "add_new"))))))

(let [elements (byclass "collapsible")]
  (doseq [elem elements]
    (.addEventListener elem "click" collapser)))
