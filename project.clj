(def buildmapdev
  [{; The path to the top-level ClojureScript source directory:
    :source-paths ["src/cljs"]
    ; The standard ClojureScript compiler options:
    ; (See the ClojureScript compiler documentation for details.)
    :compiler {:main 'todefer.habits
               :output-to "target/cljsbuild/public/js/habits.js"
               :optimizations :whitespace
               :pretty-print true}}
    {; The path to the top-level ClojureScript source directory:
     :source-paths ["src/cljs"]
     ; The standard ClojureScript compiler options:
     ; (See the ClojureScript compiler documentation for details.)
     :compiler {:main 'todefer.tasks
                :output-to "target/cljsbuild/public/js/tasks.js"
                :optimizations :whitespace
                :pretty-print true}}])

;; prod version of the build map, with optimizations turned on
(def buildmapprod
  (map (fn [build] (-> build
                       (assoc-in [:compiler :optimizations] :advanced)
                       (assoc-in [:compiler :pretty-print]  false))) buildmapdev))

(defproject todefer "0.1.0-SNAPSHOT"

  :description "ToDefer is a web app for managing tasks on a \"best-effort\" basis."
  :url "https://codeberg.org/xylon/todefer"

  :dependencies [[ch.qos.logback/logback-classic "1.4.6"]
                 [clojure.java-time "1.2.0"]
                 [conman "0.9.6"]
                 [cprop "0.1.19"]
                 [expound "0.9.0"]
                 [funcool/struct "1.4.0"]
                 [json-html "0.4.7"]
                 [luminus-migrations "0.7.5"]
                 [luminus-transit "0.1.6"]
                 [luminus-undertow "0.1.17"]
                 [luminus/ring-ttl-session "0.3.3"]
                 [markdown-clj "1.11.4"]
                 [metosin/muuntaja "0.6.8"]
                 [metosin/reitit "0.6.0"]
                 [metosin/ring-http-response "0.9.3"]
                 [mount "0.1.17"]
                 [nrepl "1.0.0"]
                 [org.clojure/clojure "1.11.1"]
                 [org.clojure/tools.cli "1.0.214"]
                 [org.clojure/tools.logging "1.2.4"]
                 [org.postgresql/postgresql "42.6.0"]
                 [org.webjars.npm/bulma "0.9.4"]
                 [org.webjars.npm/material-icons "1.13.2"]
                 [org.webjars/webjars-locator "0.46"]
                 [org.webjars/webjars-locator-jboss-vfs "0.1.0"]
                 [ring-webjars "0.2.0"]
                 [ring/ring-core "1.10.0"]
                 [ring/ring-defaults "0.3.4"]
                 [selmer "1.12.58"]
                 [buddy/buddy-hashers "1.8.158"]
                 [funcool/cuerdas "2.2.0"]
                 [org.clojure/clojurescript "1.11.60"]]

  :min-lein-version "2.0.0"
  
  :source-paths ["src/clj"]
  :test-paths ["test/clj"]
  :resource-paths ["resources" "target/cljsbuild"]
  :target-path "target/%s/"
  :main ^:skip-aot todefer.core

  :plugins [[lein-cljsbuild "1.1.8"]]
  :hooks [leiningen.cljsbuild]

  :profiles
  {:uberjar {:omit-source true
             :aot :all
             :uberjar-name "todefer.jar"
             :source-paths ["env/prod/clj" ]
             :resource-paths ["env/prod/resources"]
             :cljsbuild {:builds ~buildmapprod}}

   :dev           [:project/dev :profiles/dev]
   :test          [:project/dev :project/test :profiles/test]

   :project/dev  {:jvm-opts ["-Dconf=dev-config.edn" ]
                  :dependencies [[org.clojure/tools.namespace "1.4.4"]
                                 [pjstadig/humane-test-output "0.11.0"]
                                 [prone "2021-04-23"]
                                 [ring/ring-devel "1.10.0"]
                                 [ring/ring-mock "0.4.0"]]
                  :plugins      [[com.jakemccrary/lein-test-refresh "0.24.1"]
                                 [jonase/eastwood "1.2.4"]
                                 [cider/cider-nrepl "0.26.0"]] 
                  
                  :source-paths ["env/dev/clj" ]
                  :resource-paths ["env/dev/resources"]
                  :repl-options {:init-ns user
                                 :timeout 120000}
                  :injections [(require 'pjstadig.humane-test-output)
                               (pjstadig.humane-test-output/activate!)]
                  :cljsbuild {:builds ~buildmapdev}}
   :project/test {:jvm-opts ["-Dconf=test-config.edn" ]
                  :resource-paths ["env/test/resources"]
                  :cljsbuild {:builds ~buildmapdev}}
   :profiles/dev {}
   :profiles/test {}})
