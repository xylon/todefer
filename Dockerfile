FROM openjdk:8-alpine

COPY target/uberjar/todefer.jar /todefer/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/todefer/app.jar"]
