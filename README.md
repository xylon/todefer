# ToDefer

**NOTE: This is the old version. See the new version [on GitHub](https://github.com/Xylon2/todefer/)**

ToDefer is a web app for managing tasks on a "best-effort" basis.

It's based on [Luminus](https://luminusweb.com/) and I designed it to work with [PostgreSQL](https://www.postgresql.org/).

## Manifesto

### What problem it seeks to solve

When one makes a list of things one would like to do, it often
becomes absurdely long. Easily with dozens of tasks on it. And
then it just becomes intimidating.

Furthermore there is likely several items on the list that cannot be
done right now. Maybe that task can only be done during working hours
(call bank or go shopping), or that task can only be done after I get
payed etc.

The idea is therefore to stash things away into deferred categories so
that one has a short, managable list of things to pay-attention-to
today.

### What ToDefer provides

ToDefer allows one to define named pages containing either
"Tasks" or "Habits".

On Tasks pages, one defers tasks either into named categories, or
to specific dates. The tasks then dissapear into their categories
until either one brings the out manually or they become due
(in-the-case of date categories). The due tasks are shown at the
top of the page.

On habits pages, each "Habit" has a recurrance frequency. Habits
that are not due are hidden away. Habits that are due are shown
at the top of the page to be dealt with.

Note that Habits recurr from the date they are marked done, not the
date they were due to be done.

### What it's not

ToDefer is not a calendar. It is not for recording appointments
or holidays or yoga sessions. It maintains lists of tasks.

ToDefer is not a deadline management system. It is actually
kind-of like a time-reversed version of a deadline-management
system. It's not "this needs to be done by 12th March and I'd
like to be reminded a week in advance". It's more like "I don't
want to think about this until after the 12th March".

## Running this code

Here I explain briefly how you may run this code on your workstation for development.

Install:
- [Leiningen](https://codeberg.org/leiningen/leiningen)
- [PostgreSQL](https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e)

Create a PostgreSQL database and user, and create a file `dev-config.edn` with credentials. An example of how that might look:
```
{:dev true
 :port 3000
 ;; when :nrepl-port is set the application starts the nREPL server on load
 :nrepl-port 7000
 
 ; set your dev database connection URL here
 :database-url "postgresql://localhost/dbname?user=dbuser&password=dbpass"
}
```

To start a web server for the application, run:
```
lein repl
(migrate)
(start)
```

Now you should be able to access the app at http://localhost:3000/.

To create credentials to login type these in the repl (after running `(start)`):
```
(in-ns 'todefer.authfunctions)
 (create-user! "youruser" "yourpass")
```

n.b. ToDefer is not multi-user so if you make multiple logins they will all see the same content.

## Hosting

If you are willing to comply with the requirements of the GNU GPL you may use this code for yourself.

For general info on hosting Luminus apps check out [the luminus docs](https://luminusweb.com/docs/deployment.html).

To create an admin login, the .jar can be called with an argument "add-user". Here is an example:
```
set -o allexport
source /var/todefer/env
/usr/bin/java -jar /var/todefer/todefer.jar add-user
```

It will prompt you for a username and password.

## License

Copyright © 2022,2023 Joseph Graham

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
